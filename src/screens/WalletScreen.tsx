import React from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import {useFocusEffect, useNavigation} from '@react-navigation/native';
import {Account, Address} from '@elrondnetwork/erdjs/out';
import {networkProvider, reduceDecimals} from '../helpers';
import Button from '../components/Button';
import {ActivityIndicator} from 'react-native';
import Info from '../components/Info';
import {getWalletAddress} from '../redux/account/selectors';
import {useSelector} from 'react-redux';
import {format} from 'date-fns';

export const baseUrl = 'https://testnet-api.elrond.com/accounts/';

const WalletScreen = () => {
  const navigation = useNavigation();
  const address = useSelector(getWalletAddress);
  const [account, setAccount] = React.useState<Account | null>(null);
  const [transactions, setTransactions] = React.useState<any[]>([]);
  const [loading, setLoading] = React.useState(true);

  const getAccount = React.useCallback(async () => {
    let addressOfUser = new Address(address);
    let user = new Account(addressOfUser);

    let userOnNetwork = await networkProvider.getAccount(addressOfUser);
    user.update(userOnNetwork);

    setAccount(user);
  }, [address]);

  const getTransactions = React.useCallback(async () => {
    const response = await fetch(`${baseUrl}${address}/transactions`);

    const responseJson = await response.json();
    const filteredTransactions = responseJson.filter(
      (transaction: any) => transaction.status === 'success',
    );
    setTransactions(filteredTransactions);
  }, [address]);

  const navigateToSend = React.useCallback(() => {
    return navigation.navigate('SendTransactionScreen');
  }, [navigation]);

  useFocusEffect(
    React.useCallback(() => {
      getAccount();
      getTransactions();
      setLoading(false);
    }, [getAccount, getTransactions]),
  );

  const transactionIsReceived = React.useCallback(
    (transaction: any) => {
      return transaction.receiver === address;
    },
    [address],
  );

  if (loading && !account) {
    return (
      <View style={[styles.container, {justifyContent: 'center'}]}>
        <ActivityIndicator />
      </View>
    );
  }

  return (
    <ScrollView style={styles.scrollView}>
      <View style={styles.container}>
        <Text style={styles.title}>Wallet</Text>
        <View style={styles.inputContainer}>
          <Info label={'Address:'} value={address} />
          <View style={{marginTop: 20}} />
          <Info
            label={'Balance:'}
            value={
              reduceDecimals(account?.balance, 18).toFixed(4).toString() +
              ' XeGLD'
            }
          />
          <Button onPress={navigateToSend} text={'Send transaction'} />
          <Text
            style={{
              fontSize: 14,
              marginTop: 30,
            }}>
            Last transactions for account:
          </Text>
        </View>
        <View style={styles.transactionsContainer}>
          {transactions.map((transaction, index) => (
            <View key={index}>
              {transactionIsReceived(transaction) ? (
                <Text>{'Received from: ' + transaction.sender}</Text>
              ) : (
                <Text>{'Send to: ' + transaction.receiver}</Text>
              )}
              <View style={styles.transactionInfo}>
                <Text>{reduceDecimals(transaction.value, 18) + ' ExGLD'}</Text>
                <Text>
                  {format(new Date(transaction.timestamp * 1000), 'dd-MM-yyyy')}
                </Text>
              </View>
              <View style={styles.divider} />
            </View>
          ))}
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 20,
    flex: 1,
  },
  divider: {
    borderWidth: 0.5,
    marginVertical: 10,
    borderColor: 'rgba(0, 0, 0, 0.5)',
  },
  transactionInfo: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 5,
  },
  transactionsContainer: {
    flex: 1,
    width: '100%',
    paddingHorizontal: 20,
  },
  scrollView: {
    flex: 1,
  },
  input: {
    width: '100%',
    height: 200,
    backgroundColor: 'rgb(217, 217, 217)',
    alignSelf: 'center',
    borderRadius: 4,
    padding: 10,
  },
  title: {
    fontSize: 18,
    color: 'black',
    fontWeight: 'bold',
  },
  wordsText: {
    marginBottom: 10,
  },
  inputContainer: {
    width: '100%',
    padding: 20,
    alignItems: 'center',
  },
  buttonContainer: {
    alignItems: 'center',
    marginTop: 10,
  },
});

export default WalletScreen;
