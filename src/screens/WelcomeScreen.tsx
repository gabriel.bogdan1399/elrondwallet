import {Mnemonic, UserSecretKey} from '@elrondnetwork/erdjs-walletcore/out';
import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {Alert, StyleSheet, Text, TextInput, View} from 'react-native';
import {useDispatch} from 'react-redux';
import Button from '../components/Button';
import {getMnemonicsWords} from '../helpers';
import {setSecretKey, setWalletAddress} from '../redux/account/actions';

const WelcomeScreen = () => {
  const [value, setValue] = React.useState('');
  const [loading, setLoading] = React.useState(false);
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const getWalletAddress = React.useCallback(() => {
    setLoading(true);
    const mnemonicString = getMnemonicsWords(value);
    const mnemonic = (() => {
      try {
        return Mnemonic.fromString(mnemonicString);
      } catch (e) {
        Alert.alert('Wrong mnemonic format');
        return null;
      }
    })();

    if (mnemonic) {
      const hex = mnemonic.deriveKey(0).hex();
      const secretkey = new UserSecretKey(Buffer.from(hex, 'hex'));
      const publicKey = secretkey.generatePublicKey().toAddress().toString();

      dispatch(setSecretKey(secretkey));
      dispatch(setWalletAddress(publicKey));

      navigation.navigate('Wallet', {
        screen: 'WalletScreen',
      });
    }
  }, [dispatch, navigation, value]);

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Welcome</Text>
      <View style={styles.inputContainer}>
        <Text style={styles.wordsText}>24 words</Text>
        <TextInput
          style={styles.input}
          multiline={true}
          onChangeText={setValue}
          value={value}
        />
        <View style={styles.buttonContainer}>
          <Button
            onPress={getWalletAddress}
            text={'Import wallet'}
            loading={loading}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 20,
    flex: 1,
  },
  input: {
    width: '100%',
    height: 200,
    backgroundColor: 'rgb(217, 217, 217)',
    alignSelf: 'center',
    borderRadius: 4,
    padding: 10,
  },
  title: {
    fontSize: 18,
    color: 'black',
    fontWeight: 'bold',
  },
  wordsText: {
    marginBottom: 10,
  },
  inputContainer: {
    width: '100%',
    marginTop: 20,
    padding: 20,
  },
  buttonContainer: {
    alignItems: 'center',
    marginTop: 10,
  },
});

export default WelcomeScreen;
