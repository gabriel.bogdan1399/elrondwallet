import {useNavigation, useRoute} from '@react-navigation/native';
import React from 'react';
import {Image, Linking, StyleSheet, Text, View} from 'react-native';
import Button from '../components/Button';
import Info from '../components/Info';
import CheckIcon from '../assets/check.png';

const SuccessScreen = () => {
  const navigation = useNavigation();
  const {params}: any = useRoute();

  const navigateToWallet = React.useCallback(() => {
    navigation.navigate('WalletScreen');
  }, [navigation]);

  const exporeTx = React.useCallback(() => {
    Linking.openURL(
      `https://testnet-explorer.elrond.com/transactions/${params.txHash}`,
    );
  }, [params.txHash]);

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Confirmation</Text>
      <Image style={styles.image} source={CheckIcon} />
      <View style={styles.inputContainer}>
        <Text style={styles.amount}>{params.value + ' XeGLD'}</Text>
        <Info label={'successfully sent to:'} value={params.to} />
        <View style={{marginTop: 20}} />
        <Info label={'TX hash:'} value={params.txHash} />
        <Text onPress={exporeTx} style={styles.explorerText}>
          View in explorer
        </Text>
        <Button onPress={navigateToWallet} text={'Back to walllet'} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 20,
    flex: 1,
  },
  image: {
    width: 60,
    height: 60,
    marginVertical: 30,
    alignSelf: 'center',
  },
  explorerText: {
    textDecorationLine: 'underline',
    color: 'rgb(72, 106, 189)',
    marginVertical: 10,
    padding: 20,
  },
  input: {
    width: '100%',
    height: 200,
    backgroundColor: 'rgb(217, 217, 217)',
    alignSelf: 'center',
    borderRadius: 4,
    padding: 10,
  },
  title: {
    fontSize: 18,
    color: 'black',
    fontWeight: 'bold',
  },
  amount: {
    fontSize: 14,
    color: 'black',
    fontWeight: 'bold',
    marginBottom: 5,
  },
  wordsText: {
    marginBottom: 10,
  },
  inputContainer: {
    width: '100%',
    padding: 20,
    alignItems: 'center',
  },
  buttonContainer: {
    alignItems: 'center',
    marginTop: 10,
  },
});

export default SuccessScreen;
