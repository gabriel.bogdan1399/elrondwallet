import {UserSigner} from '@elrondnetwork/erdjs-walletcore/out';
import {
  Account,
  Address,
  TokenPayment,
  Transaction,
  TransactionVersion,
} from '@elrondnetwork/erdjs/out';
import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {StyleSheet, Text, View, TextInput, Alert} from 'react-native';
import {useSelector} from 'react-redux';
import Button from '../components/Button';
import {expandDecimals, networkProvider} from '../helpers';
import {getSecretKey, getWalletAddress} from '../redux/account/selectors';

const SendTransactionScreen = () => {
  const [to, setTo] = React.useState('');
  const [value, setValue] = React.useState('');
  const navigation = useNavigation();
  const address = useSelector(getWalletAddress);
  const secretKey = useSelector(getSecretKey);

  const sendTransactionCallBack = React.useCallback(async () => {
    let signer = new UserSigner(secretKey);
    const sender = new Address(address);
    let senderAccount = new Account(sender);
    let network = await networkProvider.getAccount(sender);
    senderAccount.update(network);

    const parsedValue = parseFloat(value.replace(/,/g, ''));

    if (expandDecimals(parsedValue, 18) > senderAccount.balance) {
      return Alert.alert('Insufficient funds');
    }

    let transaction = new Transaction({
      nonce: 0,
      value: TokenPayment.egldFromAmount(parsedValue).toString(),
      receiver: new Address(to),
      sender: sender,
      gasPrice: 1000000000,
      gasLimit: 70000,
      chainID: 'T',
      version: new TransactionVersion(1),
    });

    transaction.setNonce(senderAccount.getNonceThenIncrement());
    await signer.sign(transaction);

    const response = await fetch(
      'https://testnet-api.elrond.com/transactions',
      {
        method: 'POST',
        body: JSON.stringify(transaction.toSendable()),
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      },
    );

    const responseJson = await response.json();
    return navigation.navigate('SuccessScreen', {
      value: parsedValue,
      to: to,
      txHash: responseJson.txHash,
    });
  }, [address, navigation, secretKey, to, value]);

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Send</Text>
      <View style={styles.inputContainer}>
        <Text>To:</Text>
        <TextInput style={styles.input} onChangeText={setTo} value={to} />
        <View style={{marginTop: 10}} />
        <Text>Amount:</Text>
        <TextInput style={styles.input} onChangeText={setValue} value={value} />
      </View>
      <Button onPress={sendTransactionCallBack} text={'Send transaction'} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 20,
    flex: 1,
  },
  inputContainer: {
    marginTop: 20,
    width: '100%',
    padding: 20,
  },
  title: {
    fontSize: 18,
    color: 'black',
    fontWeight: 'bold',
  },
  input: {
    width: '100%',
    height: 40,
    backgroundColor: 'rgb(217, 217, 217)',
    alignSelf: 'center',
    padding: 5,
    marginTop: 5,
  },
});

export default SendTransactionScreen;
