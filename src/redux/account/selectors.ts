import {createSelector} from '@reduxjs/toolkit';

export const getState = (state: {account: any}) => state.account;

export const getWalletAddress = createSelector(
  getState,
  state => state.walletAddress,
);

export const getSecretKey = createSelector(getState, state => state.secretKey);
