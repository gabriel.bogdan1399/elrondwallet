export const SET_SECRET_KEY = '@ account / SET_SECRET_KEY';
export const SET_WALLET_ADDRESS = '@ account / SET_WALLET_ADDRESS';

export const setWalletAddress = (walletAddress: any) => {
  return {
    type: SET_WALLET_ADDRESS,
    payload: walletAddress,
  };
};

export const setSecretKey = (secretKey: any) => {
  return {
    type: SET_SECRET_KEY,
    payload: secretKey,
  };
};
