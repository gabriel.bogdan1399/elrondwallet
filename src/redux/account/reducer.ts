import {SET_SECRET_KEY, SET_WALLET_ADDRESS} from './actions';

interface AccountState {
  walletAddress: string;
  secretKey: string;
}

export const initialState: AccountState = {
  walletAddress: '',
  secretKey: '',
};

export default function reducer(
  state = initialState,
  action: any,
): AccountState {
  switch (action.type) {
    case SET_SECRET_KEY:
      return {
        ...state,
        secretKey: action.payload,
      };
    case SET_WALLET_ADDRESS:
      return {
        ...state,
        walletAddress: action.payload,
      };
    default:
      return state;
  }
}
