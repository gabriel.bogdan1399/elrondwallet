import {configureStore} from '@reduxjs/toolkit';
import accountReducer from './account/reducer';

const store = configureStore({
  reducer: {
    account: accountReducer,
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export default store;
