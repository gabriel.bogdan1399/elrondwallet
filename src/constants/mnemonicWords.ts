const mnemonicWords = `1 couple
2 client
3 average
4 survey
5 agent
6 ability
7 barely
8 south
9 sponsor
10 spice
11 labor
12 energy
13 reason
14 fortune
15 village
16 loan
17 hazard
18 time
19 hawk
20 lumber
21 wrong
22 veteran
23 romance
24 mule`;

export default mnemonicWords;
