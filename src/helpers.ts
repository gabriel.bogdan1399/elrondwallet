import {ApiNetworkProvider} from '@elrondnetwork/erdjs-network-providers/out';
import BigNumber from 'bignumber.js';

export const reduceDecimals = (val: any, decimals: number) =>
  BigNumber(val).shiftedBy(-decimals);

export const expandDecimals = (val: number, decimals: number) =>
  BigNumber(val).shiftedBy(decimals).toFixed(0);

export const getMnemonicsWords = (string: string) => {
  const words = string.split('\n');
  let mnemonicsString = '';

  words.map(word => {
    const [_, w] = word.split(' ');
    mnemonicsString += w.trim() + ' ';
  });

  return mnemonicsString;
};

export const networkProvider = new ApiNetworkProvider(
  'https://testnet-api.elrond.com',
);
