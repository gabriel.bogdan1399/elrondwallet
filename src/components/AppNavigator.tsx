import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import WelcomeScreen from '../screens/WelcomeScreen';
import WalletScreen from '../screens/WalletScreen';
import SendTransactionScreen from '../screens/SendTransactionScreen';
import SuccessScreen from '../screens/SuccessScreen';

const StackNavigator = createNativeStackNavigator();
const ImportWalletStack = createNativeStackNavigator();
const WalletStack = createNativeStackNavigator();

const ImportWalletNavigator = () => {
  return (
    <ImportWalletStack.Navigator
      initialRouteName={'WelcomeScreen'}
      screenOptions={{
        headerShown: false,
        gestureEnabled: true,
      }}>
      <ImportWalletStack.Screen
        name="WelcomeScreen"
        component={WelcomeScreen}
      />
    </ImportWalletStack.Navigator>
  );
};

const WalletNavigator = () => {
  return (
    <WalletStack.Navigator
      initialRouteName={'WalletScreen'}
      screenOptions={{
        headerShown: false,
        gestureEnabled: true,
      }}>
      <WalletStack.Screen name="WalletScreen" component={WalletScreen} />
      <WalletStack.Screen
        name="SendTransactionScreen"
        component={SendTransactionScreen}
      />
      <WalletStack.Screen name="SuccessScreen" component={SuccessScreen} />
    </WalletStack.Navigator>
  );
};

const StackNavigatorComponent = () => {
  return (
    <StackNavigator.Navigator
      initialRouteName={'ImportWallet'}
      screenOptions={{
        headerShown: false,
        gestureEnabled: true,
      }}>
      <StackNavigator.Screen
        name="ImportWallet"
        component={ImportWalletNavigator}
      />
      <StackNavigator.Screen name="Wallet" component={WalletNavigator} />
    </StackNavigator.Navigator>
  );
};

const AppNavigator = () => {
  return (
    <NavigationContainer>
      <StackNavigatorComponent />
    </NavigationContainer>
  );
};

export default AppNavigator;
