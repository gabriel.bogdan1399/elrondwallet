import React from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';

const Button = ({
  text = '',
  buttonStyle = {},
  textStyle = {},
  onPress = () => {},
  loading = false,
}) => {
  return (
    <TouchableOpacity onPress={onPress} style={[styles.button, buttonStyle]}>
      {loading ? (
        <ActivityIndicator size="small" color="white" />
      ) : (
        <Text style={[styles.text, textStyle]}>{text}</Text>
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    backgroundColor: 'rgb(47, 115, 224)',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20,
    borderRadius: 8,
  },
  text: {
    color: 'white',
  },
});

export default Button;
