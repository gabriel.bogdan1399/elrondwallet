import React from 'react';
import {Text} from 'react-native';

const Info = ({label, value}: {label: string; value: string}) => {
  return (
    <>
      <Text style={{color: 'rgb(110, 110, 110)', fontSize: 14}}>{label}</Text>
      <Text
        style={{
          fontSize: 14,
          marginTop: 5,
          textAlign: 'center',
          paddingHorizontal: 15,
        }}>
        {value}
      </Text>
    </>
  );
};

export default Info;
